# README

## What is Riverline\PredaddyBundle

``Riverline\PredaddyBundle`` help to integrate predaddy components into a Symfony2 application.

## Requirements

* PHP 7.x
* Symfony 2.x

## Installation

``Riverline\PredaddyBundle`` is compatible with composer and any psr-4 autoloader

## Configuration

```yml
riverline_predaddy:
    command_bus:
        app_command_bus_1:                                    # Specify multiple command bus names
            transaction_manager: predaddy.transaction_manager # Transaction manager service id

    event_bus:
        app_event_bus_1:                                      # Specify multiple event bus names
            transaction_manager: predaddy.transaction_manager # Transaction manager service id
        app_event_bus_2:                                      # Specify multiple event bus names
            transaction_manager: predaddy.transaction_manager # Transaction manager service id
            
    transaction_manager:
        class: trf4php\doctrine\DoctrineTransactionManager # (default value)
        entity_manager: doctrine.orm.entity_manager        # Doctrine entity manager service id (default value)
        
    annotation_reader: annotation_reader                   # (default value)
```

You can use command_bus in your services with following services ids :

```php
$this->getContainer()->get("riverline.predaddy_bundle.command_bus.app_command_bus_1")    
```

And for event buses :
    
```php
$this->getContainer()->get("riverline.predaddy_bundle.command_bus.app_command_bus_1")    
```

## Logging

``Riverline\PredaddyBundle`` add Symfony2 'logger' service as root logger (supports only Monolog logger).

## Usage

Define command and event handler as container service, and add tag to register to buses.

```yml
services:

  ## EVENT HANDLERS
    my.cqrs.event_handler.something:
      class: My\Vendor\SomethingEventHandler
      tags:
        -  { name: predaddy.event_handler, channel: command_bus_1 } # Channel is optionnel only if you have only once command bus

  ## COMMAND HANDLERS
    my.cqrs.command_handler.something:
      class: My\Vendor\SomethingEventHandler
      tags:
        -  { name: predaddy.command_handler, channel: event_bus_2 } # Channel is optionnel only if you have only once event bus
```

## Tests
    
    $ ./vendor/bin/phpunit

## Limitations / Known issues

* Works only with Predaddy 1.2
* Supports only Monolog logger
* And probably other ...

## License

See Resources/meta/LICENSE
