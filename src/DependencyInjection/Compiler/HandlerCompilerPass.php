<?php

namespace Riverline\PredaddyBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class HandlerCompilerPass
 * @package Riverline\PredaddyBundle\DependencyInjection\Compiler
 */
class HandlerCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        // Command handlers
        foreach ($container->findTaggedServiceIds('predaddy.command_handler') as $id => $attributes) {
            foreach ($attributes as $attribute) {
                $definition = array_key_exists("channel", $attribute) === true
                    ? $container->getDefinition('riverline.predaddy_bundle.command_bus.'.$attribute["channel"])
                    : $container->getDefinition(
                        $container->getAlias('riverline.predaddy_bundle.command_bus_default')
                    );

                $definition->addMethodCall(
                    'register',
                    [new Reference($id)]
                );
            }
        }

        // Event handlers
        foreach ($container->findTaggedServiceIds('predaddy.event_handler') as $id => $attributes) {
            foreach ($attributes as $attribute) {
                $definition = array_key_exists("channel", $attribute) === true
                    ? $container->getDefinition('riverline.predaddy_bundle.event_bus.'.$attribute["channel"])
                    : $container->getDefinition(
                        $container->getAlias('riverline.predaddy_bundle.event_bus_default')
                    );

                $definition->addMethodCall(
                    'register',
                    [new Reference($id)]
                );
            }
        }
    }
}
