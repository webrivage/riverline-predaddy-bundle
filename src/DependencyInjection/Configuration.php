<?php

namespace Riverline\PredaddyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package Riverline\PredaddyBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('riverline_predaddy');

        $rootNode
            ->children()
                ->arrayNode('command_bus')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('transaction_manager')->defaultValue('predaddy.transaction_manager')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('event_bus')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('transaction_manager')->defaultValue('predaddy.transaction_manager')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('transaction_manager')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('class')->defaultValue('trf4php\doctrine\DoctrineTransactionManager')->end()
                        ->scalarNode('entity_manager')->defaultValue('doctrine.orm.entity_manager')->end()
                    ->end()
                ->end()
                ->arrayNode('logging')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('type')->defaultValue('monolog')->end()
                        ->scalarNode('monolog_logger')->defaultValue('logger')->end()
                    ->end()
                ->end()
                ->scalarNode("annotation_reader")->defaultValue("annotation_reader")->end()
            ->end();

        return $treeBuilder;
    }
}
