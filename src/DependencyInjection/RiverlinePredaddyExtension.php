<?php

namespace Riverline\PredaddyBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class RiverlinePredaddyExtension
 * @package Riverline\PredaddyBundle\DependencyInjection
 */
class RiverlinePredaddyExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = $this->processConfiguration(new Configuration(), $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        // Command bus
        foreach ($configuration['command_bus'] as $name => $commandBusConfiguration) {
            $definition = new DefinitionDecorator("predaddy.command_bus");
            $definition->replaceArgument(3, new Reference($commandBusConfiguration['transaction_manager']));

            $container->setDefinition("riverline.predaddy_bundle.command_bus.".$name, $definition);

            if (count($configuration['command_bus']) === 1) {
                $container->setAlias(
                    'riverline.predaddy_bundle.command_bus_default',
                    "riverline.predaddy_bundle.command_bus.".$name
                );
            }
        }

        // Event bus
        foreach ($configuration['event_bus'] as $name => $eventBusConfiguration) {
            $definition = new DefinitionDecorator("predaddy.event_bus");
            $definition->replaceArgument(3, new Reference($eventBusConfiguration['transaction_manager']));

            $container->setDefinition("riverline.predaddy_bundle.event_bus.".$name, $definition);

            if (count($configuration["event_bus"]) === 1) {
                $container->setAlias(
                    'riverline.predaddy_bundle.event_bus_default',
                    "riverline.predaddy_bundle.event_bus.".$name
                );
            }
        }

        // Transaction manager
        $container->setParameter('predaddy.transaction_manager.class', $configuration['transaction_manager']['class']);
        if ($configuration['transaction_manager']['entity_manager'] != 'doctrine.orm.entity_manager') {
            $transactionManagerDefinition = $container->findDefinition('predaddy.transaction_manager');
            $transactionManagerDefinition->replaceArgument(0, new Reference($configuration['transaction_manager']['entity_manager']));
        }

        // Logging
        if ($configuration['logging']['type'] != 'monolog') {
            throw new \RuntimeException("Unmanaged logging type {$configuration['logging']['type']}");
        }

        // Annotation Reader
        if ($configuration['annotation_reader'] != 'annotation_reader') {
            $annotatedMessageHandlerDescriptorFactoryDefinition = $container->findDefinition('predaddy.command_message_handler_descriptor_factory');
            $annotatedMessageHandlerDescriptorFactoryDefinition->replaceArgument(1, new Reference($configuration['annotation_reader']));

            $annotatedMessageHandlerDescriptorFactoryDefinition = $container->findDefinition('predaddy.event_message_handler_descriptor_factory');
            $annotatedMessageHandlerDescriptorFactoryDefinition->replaceArgument(1, new Reference($configuration['annotation_reader']));
        }
    }
}
