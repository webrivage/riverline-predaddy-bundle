<?php

namespace Riverline\PredaddyBundle\Logging;

use lf4php\CachedClassLoggerFactory;
use lf4php\monolog\MonologLoggerWrapper;
use Monolog\Logger;

class MonologLoggerFactory extends CachedClassLoggerFactory
{

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        parent::__construct(new MonologLoggerWrapper($logger));
    }

} 