<?php

namespace Riverline\PredaddyBundle\MessageHandling;

use Exception;
use predaddy\messagehandling\MessageCallback;

class CommandMessageCallback implements MessageCallback
{

    /**
     * @var Exception
     */
    private $exception;

    /**
     * @var bool
     */
    private $isSuccess = false;

    /**
     * @var mixed
     */
    private $result;

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->isSuccess;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return \Exception
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * {@inheritdoc}
     */
    public function onSuccess($result)
    {
        $this->isSuccess = true;
        $this->result    = $result;
    }

    /**
     * {@inheritdoc}
     */
    public function onFailure(Exception $exception)
    {
        $this->exception = $exception;
    }
} 