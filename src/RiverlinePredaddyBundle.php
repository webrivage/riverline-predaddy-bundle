<?php

namespace Riverline\PredaddyBundle;

use lf4php\LoggerFactory;
use Riverline\PredaddyBundle\DependencyInjection\Compiler\HandlerCompilerPass;
use Riverline\PredaddyBundle\Logging\MonologLoggerFactory;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RiverlinePredaddyBundle
 * @package Riverline\PredaddyBundle
 */
class RiverlinePredaddyBundle extends Bundle
{

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        parent::boot();

        if (true === $this->container->has("logger")) {
            LoggerFactory::setILoggerFactory(new MonologLoggerFactory($this->container->get('logger')));
        }
    }

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new HandlerCompilerPass(), PassConfig::TYPE_AFTER_REMOVING);
    }

}
