<?php

namespace Riverline\PredaddyBundle;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class AbstractPredaddyBundleTest
 *
 * @package Riverline\PredaddyBundle
 */
abstract class AbstractPredaddyBundleTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var AppKernel
     */
    protected $kernel;

    /**
     *
     */
    public function setUp()
    {
        $fs = new Filesystem();
        $fs->remove(__DIR__.'../cache');

        $this->kernel = new AppKernel('env', true);
        $this->kernel->boot();
    }
}
