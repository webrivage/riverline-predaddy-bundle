<?php

namespace Riverline\PredaddyBundle\Bundle;

use Riverline\PredaddyBundle\AbstractPredaddyBundleTest;

/**
 * Class RiverlinePredaddyBundleTest
 * @package Riverline\PredaddyBundle\Bundle
 */
class RiverlinePredaddyBundleTest extends AbstractPredaddyBundleTest
{
    /**
     *
     */
    public function testShouldBootBundle()
    {
        $this->assertTrue($this->kernel->getContainer()->has("riverline.predaddy_bundle.command_bus.your_event_bus_name"));
        $this->assertTrue($this->kernel->getContainer()->has("riverline.predaddy_bundle.command_bus.your_event_bus_name_2"));
        $this->assertTrue($this->kernel->getContainer()->has("riverline.predaddy_bundle.event_bus.your_event_bus_name"));
        $this->assertTrue($this->kernel->getContainer()->has("riverline.predaddy_bundle.event_bus.your_event_bus_name_2"));
    }
}
