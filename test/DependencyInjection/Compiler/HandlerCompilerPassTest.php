<?php

namespace Riverline\PredaddyBundle\DependencyInjection\Compiler;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class HandlerCompilerPassTest
 * @package DependencyInjection\Compiler
 */
class HandlerCompilerPassTest extends AbstractCompilerPassTestCase
{
    /**
     *
     */
    public function testShouldRegisterCommandHandlers()
    {
        $collectingService = new Definition();
        $this->setDefinition('riverline.predaddy_bundle.command_bus.main', $collectingService);
        $this->container->setAlias('riverline.predaddy_bundle.command_bus_default', 'riverline.predaddy_bundle.command_bus.main');

        $collectedService = new Definition();
        $collectedService->addTag("predaddy.command_handler");
        $this->setDefinition("command_handler", $collectedService);

        $this->compile();

        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'riverline.predaddy_bundle.command_bus_default',
            'register',
            [
                new Reference('command_handler'),
            ]
        );
    }

    /**
     *
     */
    public function testShouldRegisterCommandHandlersWithSpecificChannel()
    {
        $this->setDefinition('riverline.predaddy_bundle.command_bus.command_bus_1', new Definition());
        $this->setDefinition('riverline.predaddy_bundle.command_bus.command_bus_2', new Definition());

        $collectedService = new Definition();
        $collectedService->addTag("predaddy.command_handler", ["channel" => "command_bus_2"]);
        $this->setDefinition("command_handler", $collectedService);

        $this->compile();

        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'riverline.predaddy_bundle.command_bus.command_bus_2',
            'register',
            [
                new Reference('command_handler'),
            ]
        );
    }

    /**
     *
     */
    public function testShouldRegisterEventHandlers()
    {
        $collectingService = new Definition();
        $this->setDefinition('riverline.predaddy_bundle.event_bus.main', $collectingService);
        $this->container->setAlias('riverline.predaddy_bundle.event_bus_default', 'riverline.predaddy_bundle.event_bus.main');

        $collectedService = new Definition();
        $collectedService->addTag("predaddy.event_handler");
        $this->setDefinition("event_handler", $collectedService);

        $this->compile();

        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'riverline.predaddy_bundle.event_bus_default',
            'register',
            [
                new Reference('event_handler'),
            ]
        );
    }

    /**
     *
     */
    public function testShouldRegisterEventHandlersWithSpecificChannel()
    {
        $this->setDefinition('riverline.predaddy_bundle.event_bus.event_bus_1', new Definition());
        $this->setDefinition('riverline.predaddy_bundle.event_bus.event_bus_2', new Definition());

        $collectedService = new Definition();
        $collectedService->addTag("predaddy.event_handler", ["channel" => "event_bus_1"]);
        $this->setDefinition("event_handler", $collectedService);

        $this->compile();

        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'riverline.predaddy_bundle.event_bus.event_bus_1',
            'register',
            [
                new Reference('event_handler'),
            ]
        );
    }

    /**
     *
     */
    public function testShouldThrowExceptionIfNoChannelSpecifiedAndManyCommandBus()
    {
        $this->setDefinition("riverline.predaddy_bundle.command_bus.command_bus_1", new Definition());
        $this->setDefinition("riverline.predaddy_bundle.command_bus.command_bus_2", new Definition());

        $collectedService = new Definition();
        $collectedService->addTag("predaddy.command_handler");
        $this->setDefinition("command_handler", $collectedService);

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("The service alias \"riverline.predaddy_bundle.command_bus_default\" does not exist.");
        $this->compile();
    }

    /**
     *
     */
    public function testShouldThrowExceptionIfNoChannelSpecifiedAndManyEventBus()
    {
        $this->setDefinition("riverline.predaddy_bundle.event_bus.event_bus_1", new Definition());
        $this->setDefinition("riverline.predaddy_bundle.event_bus.event_bus_2", new Definition());

        $collectedService = new Definition();
        $collectedService->addTag("predaddy.event_handler");
        $this->setDefinition("event_handler", $collectedService);

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("The service alias \"riverline.predaddy_bundle.event_bus_default\" does not exist.");
        $this->compile();
    }

    /**
     * @param ContainerBuilder $container
     */
    protected function registerCompilerPass(ContainerBuilder $container)
    {
        $container->addCompilerPass(new HandlerCompilerPass());
    }
}
