<?php

namespace Riverline\PredaddyBundle\DependencyInjection;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use predaddy\messagehandling\command\CommandBus;
use predaddy\messagehandling\event\EventBus;
use trf4php\doctrine\DoctrineTransactionManager;

/**
 * Class RiverlinePredaddyExtensionTest
 * @package DependencyInjection
 */
class RiverlinePredaddyExtensionTest extends AbstractExtensionTestCase
{
    /**
     *
     */
    public function testShouldDefineCommandAndEventBusServicesWithConfiguration()
    {
        $this->load([
            "command_bus"         => [
                "app_command_bus"   => null,
                "app_command_bus_2" => [
                    "transaction_manager" => "some.transaction_manager.id",
                ],
            ],
            "event_bus"           => [
                "app_event_bus" => null,
            ],
            "transaction_manager" => [
                "class"          => \stdClass::class,
                "entity_manager" => "app-entity_manager",
            ],
            "annotation_reader"   => "app-annotation-reader",
        ]);

        //Check annotation reader
        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            "predaddy.command_message_handler_descriptor_factory",
            1,
            "app-annotation-reader"
        );
        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            "predaddy.event_message_handler_descriptor_factory",
            1,
            "app-annotation-reader"
        );

        //Check service command bus
        $this->assertContainerBuilderHasServiceDefinitionWithParent("riverline.predaddy_bundle.command_bus.app_command_bus", "predaddy.command_bus");
        $this->assertContainerBuilderHasServiceDefinitionWithParent("riverline.predaddy_bundle.command_bus.app_command_bus", "predaddy.command_bus");
        $this->assertContainerBuilderHasServiceDefinitionWithParent("riverline.predaddy_bundle.command_bus.app_command_bus_2", "predaddy.command_bus");
        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            "riverline.predaddy_bundle.command_bus.app_command_bus_2",
            3,
            "some.transaction_manager.id"
        );

        //Check service event bus
        $this->assertContainerBuilderHasServiceDefinitionWithParent("riverline.predaddy_bundle.event_bus.app_event_bus", "predaddy.event_bus");

        //Check transaction manager
        $this->assertContainerBuilderHasParameter('predaddy.transaction_manager.class', \stdClass::class);
        $this->assertContainerBuilderHasServiceDefinitionWithArgument('predaddy.transaction_manager', 0, "app-entity_manager");
    }

    /**
     *
     */
    public function testShouldDefineCommandAndEventBusServicesWithDefaultConfiguration()
    {
        $this->load([
            "command_bus" => [
                "app_command_bus" => null,
            ],
            "event_bus"   => [
                "app_event_bus"   => null,
                "app_event_bus_2" => null,
            ],
        ]);

        //Check annotation reader
        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            "predaddy.command_message_handler_descriptor_factory",
            1,
            "annotation_reader"
        );
        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            "predaddy.event_message_handler_descriptor_factory",
            1,
            "annotation_reader"
        );

        //Check service command bus
        $this->assertContainerBuilderHasServiceDefinitionWithParent("riverline.predaddy_bundle.command_bus.app_command_bus", "predaddy.command_bus");

        //Check service event bus
        $this->assertContainerBuilderHasServiceDefinitionWithParent("riverline.predaddy_bundle.event_bus.app_event_bus", "predaddy.event_bus");
        $this->assertContainerBuilderHasServiceDefinitionWithParent("riverline.predaddy_bundle.event_bus.app_event_bus_2", "predaddy.event_bus");

        //Check transaction manager
        $this->assertContainerBuilderHasParameter('predaddy.transaction_manager.class', DoctrineTransactionManager::class);
        $this->assertContainerBuilderHasServiceDefinitionWithArgument('predaddy.transaction_manager', 0, "doctrine.orm.entity_manager");
    }

    /**
     * {@inheritdoc}
     */
    protected function getContainerExtensions()
    {
        return [
            new RiverlinePredaddyExtension(),
        ];
    }
}
